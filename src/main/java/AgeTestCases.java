import org.junit.Test;

import com.emids.design.strategy.AgeStrategy;
import com.emids.entity.Person;

import junit.framework.TestCase;

public class AgeTestCases extends TestCase {

	private Person person;

	private double amount;

	private AgeStrategy ageStrategy;

	public AgeTestCases(String testName) {
		super(testName);
	}

	protected void setUp() throws Exception {
		super.setUp();
		person = new Person(1, "ddd", 17, "", null, null);
		amount = 5000.00;
		ageStrategy = new AgeStrategy();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		person = null;
		amount = 0;
		ageStrategy = null;
	}

	@Test
	public void testAgeNotZero() {
		person = new Person(1, "ddd", 0, "", null, null);
		String message = ageStrategy.validatePay(person, amount);
		// System.out.println(message);
		assertEquals("Age cannot be 0", message);
	}

	@Test
	public void testAgeLessThanZero() {
		person = new Person(1, "ddd", -1, "", null, null);
		String message = ageStrategy.validatePay(person, amount);
		// System.out.println(message);
		assertEquals("Age cannot be less than 0", message);
	}

	@Test
	public void testAgemoreThan110() {
		person = new Person(1, "ddd", 111, "", null, null);
		String message = ageStrategy.validatePay(person, amount);
		// System.out.println(message);
		assertEquals("Age cannot be more than 110", message);
	}

	@Test
	public void testAge1() {
		person = new Person(1, "ddd", 17, "", null, null);
		double amt = ageStrategy.pay(person, amount);
		// System.out.println(message);
		assertEquals(0.0, amt);
	}

	@Test
	public void testAge2() {
		person = new Person(1, "ddd", 18, "", null, null);
		double amt = ageStrategy.pay(person, amount);
		// System.out.println(message);
		assertEquals(5500.00, amt);
	}

	@Test
	public void testAge3() {
		person = new Person(1, "ddd", 41, "", null, null);
		double amt = ageStrategy.pay(person, amount);
		// System.out.println(message);
		assertEquals(5100.00, amt);
	}

	@Test
	public void testAge4() {
		person = new Person(1, "ddd", 26, "", null, null);
		double amt = ageStrategy.pay(person, amount);
		// System.out.println(message);
		assertEquals(6050.0, amt);
	}

	@Test
	public void testAge5() {
		person = new Person(1, "ddd", 31, "", null, null);
		double amt = ageStrategy.pay(person, amount);
		// System.out.println(message);
		assertEquals(6655.0, amt);
	}

	@Test
	public void testAge6() {
		person = new Person(1, "ddd", 36, "", null, null);
		double amt = ageStrategy.pay(person, amount);
		// System.out.println(message);
		assertEquals(7320.5, amt);
	}

	@Test
	public void testAge7() {
		person = new Person(1, "ddd", 39, "", null, null);
		double amt = ageStrategy.pay(person, amount);
		// System.out.println(message);
		assertEquals(7320.5, amt);
	}

	@Test
	public void testAmountNot0() {
		String message = ageStrategy.validatePay(person, 0.0);
		// System.out.println(message);
		assertEquals("Amount Cannot be 0", message);
	}

	@Test
	public void testAmountLessThan0() {
		String message = ageStrategy.validatePay(person, -1.00);
		// System.out.println(message);
		assertEquals("Amount Cannot be Less Than 0", message);
	}

	@Test
	public void testAmount() {
		double amt = ageStrategy.pay(person, amount);
		// System.out.println(message);
		assertEquals(0.0, amt);
	}

}
