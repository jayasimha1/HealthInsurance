package com.emids.design.strategy;

import java.util.Map;
import java.util.Set;

import com.emids.entity.Habbits;
import com.emids.entity.Person;

public class HabbitsStrategy implements Strategy{
	
	public Double pay(Person p, Double amt) {
		Map habbitsMap = p.getHabbitsMap();
		double totalPremium=amt;
		Set<Habbits> keyset = habbitsMap.keySet();
		for(Habbits habbit : keyset) {
			if((Boolean) habbitsMap.get(habbit) && !habbit.isGoodHabbit() ) {
				totalPremium+=amt*0.03;
			}
			if((Boolean) habbitsMap.get(habbit) && habbit.isGoodHabbit() ) {
				totalPremium-=amt*0.03;
			}
		}
		return totalPremium;
	}
}
