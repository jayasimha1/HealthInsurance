package com.emids.design.strategy;

import com.emids.entity.Person;

public class AgeStrategy implements Strategy {

	// @Override
	public Double pay(Person p, Double basePremium) {
		double totalPremium = basePremium;
		int age = p.getAge();
		if (age >= 18 && age < 40) {
			totalPremium += totalPremium * 0.10;
			if (age >= 25 && age < 40) {
				totalPremium += totalPremium * 0.10;
				if (age >= 30 && age < 40) {
					totalPremium += totalPremium * 0.10;
					if (age >= 35 && age < 40) {
						totalPremium += totalPremium * 0.10;
					}
				}
			}
		} else if (age >= 40 && age < 41) {
			int diff = age - 40;
			if (diff != 0) {
				int num = diff / 5;
				for (int i = 0; i < num; i++) {
					totalPremium += totalPremium / 20;
					// one more condition if the remainign value is 1,2,3,4??
				}
			}
		} else if (age == 41) {
			totalPremium += 100;
		} else {
			totalPremium = 0;
		}
		return totalPremium;
	}

	public String validatePay(Person p, Double amt) {
		String message = null;
		if (p.getAge() < 0) {
			message = "Age cannot be less than 0";
		} else if (p.getAge() == 0) {
			message = "Age cannot be 0";
		} else if (p.getAge() > 110) {
			message = "Age cannot be more than 110";
		} else if (amt == 0) {
			message = "Amount Cannot be 0";
		} else if (amt <= 0) {
			message = "Amount Cannot be Less Than 0";
		}
		return message;
	}

	/*
	 * @Override Double preProcess(Person p, Double amt) { return pay(p,amt); }
	 */
}
