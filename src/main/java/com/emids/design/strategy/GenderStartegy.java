package com.emids.design.strategy;

import com.emids.entity.Person;

public class GenderStartegy  implements Strategy{

	public Double pay(Person p,Double amt) {
		double totalPremium=0;
		if("Male".equals(p.getGender())) {
			totalPremium=(float) (amt+(amt*0.02));
		}
		return totalPremium;
	}

}
