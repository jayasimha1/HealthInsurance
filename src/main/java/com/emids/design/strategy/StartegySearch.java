package com.emids.design.strategy;

import com.emids.entity.Person;

public abstract class StartegySearch implements Strategy {

	public Double pay(Person p, Double amt) {
		double totalPremium=amt;
		while (true) {
			preProcess(p,amt);
			/*if (search()) {
				break;
			}
			postProcess();*/
		}
	}

	abstract Double preProcess(Person p,Double amt);

	//abstract boolean search();

	//abstract void postProcess();

}
