package com.emids.design.strategy;

import com.emids.entity.Person;

public interface Strategy {
	Double pay(Person p,Double amount);
}
