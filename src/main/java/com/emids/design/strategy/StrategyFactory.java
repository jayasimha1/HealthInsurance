package com.emids.design.strategy;

import com.emids.entity.StrategyEnum;

public class StrategyFactory {
	private final Strategy AgeStrategy = new AgeStrategy();
	private final Strategy GenderStrategy = new GenderStartegy();
	private final Strategy HabbitStrategy = new HabbitsStrategy();
	private final Strategy HealthStrategy = new HealthStrategy();

	public Strategy findStartegy(StrategyEnum strategyEnum) {
		Strategy strgy=null;
		switch (strategyEnum) {
		case Age:   strgy = AgeStrategy;
		case Gender:strgy = GenderStrategy;
		case Habbit:strgy = HabbitStrategy;
		case Health:strgy = HealthStrategy;
		default:
			break;
		}
		return strgy;
	}
}
