package com.emids.design.strategy;

import java.util.Map;
import java.util.Set;

import com.emids.entity.Health;
import com.emids.entity.Person;

public class HealthStrategy  implements Strategy {

	public Double pay(Person p, Double amt) {
		Map healthMap = p.getHealthMap();
		double totalPremium=amt;
		Set<Health> keyset = healthMap.keySet();
		for(Health health : keyset) {
			if((Boolean) healthMap.get(health)) {
				totalPremium+=amt*0.01;
			}
		}
		return totalPremium;
	}
}
