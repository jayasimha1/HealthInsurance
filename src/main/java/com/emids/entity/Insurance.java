package com.emids.entity;

public class Insurance {

	private int id;
	
	private Person person;
	
	private double amount;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "Insurance [id=" + id + ", person=" + person + ", amount=" + amount + "]";
	}
}
