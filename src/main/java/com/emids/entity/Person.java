package com.emids.entity;

import java.util.List;
import java.util.Map;

public class Person {

	private int id;
	
	private String name;
	
	private int age;
	
	private String gender;
	
	private Map<Habbits,Boolean> habbitsMap;
	
	private Map<Health,Boolean> healthMap;
	
	public Person(){
		
	}
	public Person(int id,String name,int age,String gender,Map habbitsMap,Map healthMap){
		this.id=id;
		this.name=name;
		this.age=age;
		this.gender=gender;
		this.habbitsMap=habbitsMap;
		this.healthMap=healthMap;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Map<Habbits, Boolean> getHabbitsMap() {
		return habbitsMap;
	}

	public void setHabbitsMap(Map<Habbits, Boolean> habbitsMap) {
		this.habbitsMap = habbitsMap;
	}

	public Map<Health, Boolean> getHealthMap() {
		return healthMap;
	}

	public void setHealthMap(Map<Health, Boolean> healthMap) {
		this.healthMap = healthMap;
	}
	
	
	
}
