package com.emids.entity;

public class Habbits {

	private int id;
	
	private String name;
	
	private boolean isGoodHabbit;
	
	Habbits(int id,String name,Boolean isGoodHabbit){
		this.id=id;
		this.name=name;
		this.isGoodHabbit = isGoodHabbit;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setGoodHabbit(boolean isGoodHabbit) {
		this.isGoodHabbit = isGoodHabbit;
	}

	public boolean isGoodHabbit() {
		return isGoodHabbit;
	}
}