package com.emids.entity;

public class Health {
	
	private int id;
	
	private String name;
	
	
	Health(int id,String name){
		this.id=id;
		this.name=name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Health [id=" + id + ", name=" + name + "]";
	}
	
	
}
