package com.emids.entity;

import java.util.HashMap;
import java.util.Map;

import com.emids.design.strategy.Strategy;
import com.emids.design.strategy.StrategyFactory;

public class InsurancePremiumTest {
	@SuppressWarnings("null")
	public static void main(String[] args) {
		Insurance insurance = new Insurance();
		Person person = new Person();
		person.setId(1);
		person.setName("Norman Gomes");
		person.setAge(34);
		person.setGender("Male");
		validatePerson(person);
		Habbits exerciseHabbit =new Habbits(1,"Daily Exercise",true);
		Habbits smokingHabbit =new Habbits(2,"Smoking",false);
		Habbits alchohalHabbit =new Habbits(3,"Consumption of alcohol",false);
		Habbits drugsHabbit =new Habbits(4,"Drugs",false);
		
		Health hypertension =  new Health(1,"hypertension");
		Health bloodPressure =  new Health(1,"bloodPressure");
		Health bloodSugar =  new Health(1,"bloodSugar");
		Health overweight =  new Health(1,"overweight");
		
		Map<Habbits,Boolean> habbitsMap = new HashMap<Habbits,Boolean>() ;
		habbitsMap.put(drugsHabbit, false);habbitsMap.put(smokingHabbit, false);
		habbitsMap.put(exerciseHabbit, true);habbitsMap.put(alchohalHabbit, true);
		person.setHabbitsMap(habbitsMap);
		
		Map<Health,Boolean> healthMap = new HashMap<Health,Boolean>() ;
		healthMap.put(hypertension, false);healthMap.put(bloodPressure, false);
		healthMap.put(bloodSugar, false);healthMap.put(overweight, true);
		person.setHealthMap(healthMap);
		
		StrategyFactory strategyFactory = new StrategyFactory();
		double amount = 5000.00;
		for (StrategyEnum str : StrategyEnum.values()) {
			Strategy strgy = strategyFactory.findStartegy(str);
			Context context = new Context(strgy);
			amount = context.executeStrategy(person,amount);
		}
		
		/*Context context = new Context(new AgeStrategy());
		double amount = context.executeStrategy(person,5000.00);
		
		context = new Context(new GenderStartegy());
		amount = context.executeStrategy(person,amount);
		
		context = new Context(new HabbitsStrategy());
		amount = context.executeStrategy(person,amount);
		
		context = new Context(new HealthStrategy());
		amount = context.executeStrategy(person,amount);*/
		
		insurance.setId(1);
		insurance.setPerson(person);
		insurance.setAmount(amount);
		System.out.println("Insurance : " + insurance);
	}
	
	public static void validatePerson(Person person) {
		if(person.getAge()<=0) {
			System.out.println("Error : Invalid Age : "+person.getAge());
		}
	}
}
