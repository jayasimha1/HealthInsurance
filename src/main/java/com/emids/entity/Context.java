package com.emids.entity;

import com.emids.design.strategy.Strategy;

public class Context {
	   private Strategy strategy;

	   public Context(Strategy strategy){
	      this.strategy = strategy;
	   }

	   public Double executeStrategy(Person p,Double amount){
	      return strategy.pay(p,amount);
	   }
	}
